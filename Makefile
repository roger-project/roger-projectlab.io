### -*-Makefile-*- to build the site of "Roger the Omni Grader"
##
## Copyright (C) 2021 Vincent Goulet
##
## Author: Vincent Goulet
##
## This file is part of Roger <https://gitlab.com/roger-project>.


## Package name and CRAN url
MAIN = roger
CRANURL = https://cran.r-project.org/package=${MAIN}

## Configuration file
CONFIG = hugo.toml

## GitLab repository and authentication
REPOSNAME = ${MAIN}-base
HEAD = master
APIURL = https://gitlab.com/api/v4/projects/roger-project%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Get the version number and date of the latest release of roger-base
## from the repository
VERSIONBASEFULL = $(shell \
  curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
       --silent \
       "${APIURL}/releases" | \
  grep -o '{"name":"[^"]*"' | \
  head -n 1 | \
  cut -d \" -f 4)
VERSIONBASE = $(word 1,${VERSIONBASEFULL})
TAGNAME = v${VERSIONBASE}

## Get the url of assets published with the release of roger-base
ASSETS = $(shell \
  curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
       --silent \
       ${APIURL}/releases/${TAGNAME}/assets/links | \
  sed 's/,/\n/g' | \
  awk 'BEGIN { FS = "\"" } \
       /^"name"/ { file = $$4 } \
       /^"direct_asset_url"/ { print file, $$4 }')
PKG_ID = $(shell echo "${ASSETS}" | \
  awk '{ for (i = 1; i <= NF; i++) if ($$i ~ /.*\.pkg/) { print $$(i + 1); exit } }')
EXE_ID = $(shell echo "${ASSETS}" | \
  awk '{ for (i = 1; i <= NF; i++) if ($$i ~ /.*\.exe/) { print $$(i + 1); exit } }')
TGZ_ID = $(shell echo "${ASSETS}" | \
  awk '{ for (i = 1; i <= NF; i++) if ($$i ~ /.*\.tar\.gz/) { print $$(i + 1); exit } }')

## Extract the version info of the R package from CRAN.
VERSIONRPKGFULL = $(shell \
  curl --silent --location "${CRANURL}" | \
  pandoc -f html -t plain | \
  awk '/^  Version:/ { version = $$2 } \
       /^  Published:/ { date = $$2 } \
       END { print version " (" date ")" }')
VERSIONRPKG = $(word 1,${VERSIONRPKGFULL})


all: config commit

config:
	@printf "updating the configuration...\n"
	@printf "  version roger-base: %s\n" "${VERSIONBASEFULL}"
	@printf "  url to pkg file: %s\n" "${PKG_ID}"
	@printf "  url to exe file: %s\n" "${EXE_ID}"
	@printf "  url to tar.gz file: %s\n" "${TGZ_ID}"
	@printf "  version roger-rpkg: %s\n" "${VERSIONRPKGFULL}"
	@awk 'BEGIN { FS = "\""; OFS = "\"" } \
	     /version_base/ { $$2 = "${VERSIONBASEFULL}" } \
	     /pkg_id/ { $$2 = "${PKG_ID}" } \
	     /exe_id/ { $$2 = "${EXE_ID}" } \
	     /tgz_id/ { $$2 = "${TGZ_ID}" } \
	     /version_rpkg/ { $$2 = "${VERSIONRPKGFULL}" } \
	     1' \
	    ${CONFIG} > tmpfile && \
	  mv tmpfile ${CONFIG}
	@printf "done\n"

commit:
	git commit ${CONFIG} \
	    -m "Updated site for v${VERSIONBASE} of roger-base and v${VERSIONRPKG} of roger-rpkg"; \
	git push
