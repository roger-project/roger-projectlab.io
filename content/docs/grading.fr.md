---
title: Guide de correction avec Roger
summary: Destiné aux correcteurs et correctrices, le guide de
  correction explique étape par étape comment effectuer la
  correction de travaux informatiques à l'aide de Roger.
type: documentation
toc: true
---

Le présent guide s'adresse aux personnes chargées de la correction de
travaux informatiques avec le système Roger. Il ne traite pas de la
préparation du matériel de correction en tant que tel (solutions,
configuration, tests unitaires).

Avant toute chose, soulignons que tous les outils de Roger sont bien
documentés. N'hésitez donc pas à utiliser `roger <outil> --help` en
cas de doute sur l'utilisation d'un outil.

Roger est particulièrement utile lorsque les travaux sont hébergés
dans un projet Git à raison d'un dépôt par équipe ou par individu.
C'est le contexte de ce guide.

# En un coup d'œil

Voici en résumé les commandes permettant d'effectuer la correction
d'une évaluation dans le contexte suivant:

- la [configuration pour l'accès au référentiel]({{< relref "#rogerrc" >}}) 
  a été effectuée;
- tous les outils requis pour la correction sont disponibles sur le
  poste de travail;
- les dépôts de l'évaluation se trouvent dans le projet `foobar`;
- les noms des dépôts correspondent au motif `[0-9]{9}_[Ll]abo`;
- la date limite de remise des travaux est le 2042-10-11 à 23:59:59;
- la correction automatisée doit être suivie d'une part de correction
  manuelle;
- les résultats de la correction sont sauvegardés dans un fichier
  `CORRECTION.txt` dans chaque dépôt;
- les résultats sont publiés dans une nouvelle branche `correction` de
  chaque dépôt.

```
roger checkreq
roger clone '[0-9]{9}_[Ll]abo' foobar
roger grade -d -l "2042-10-11 23:59:59" -o CORRECTION.txt [0-9]*/
... correction manuelle...
roger harvest [0-9]*/
roger push -c correction [0-9]*/
roger switch main [0-9]*/
```

Les sections suivantes fournissent les explications détaillées pour
chaque étape.


# Étape 0 --- Configurer l'accès au référentiel {id="rogerrc"}

Le clonage des dépôts et la publication des résultats requiert
l'[URI](https://fr.wikipedia.org/wiki/Uniform_Resource_Identifier) du
référentiel et une authentification. Il est possible de fournir ces
informations à l'outil `clone` à la ligne de commande, mais il s'avère
bien plus simple et efficace de configurer votre poste de travail une
fois pour toutes.

1. Créer dans votre répertoire personnel un fichier `.rogerrc`
   similaire à un fichier `.netrc` (voir ci-dessous), mais sans
   information d'authentification.
   
   Le fichier doit contenir deux mots-clés suivis de valeurs:
   `machine` et `api`. Le mot-clé `machine` identifie le nom d'hôte,
   le port (le cas échéant) et le contexte (le cas échéant) de l'URI
   du référentiel. Le mot-clé `api` identifie l'API REST du serveur
   Git.
   
   Pour un projet hébergé dans le [référentiel
   BitBucket](https://projets.fsg.ulaval.ca/git) de la Faculté des
   sciences et de génie de l'Université Laval, le contenu du fichier
   `.rogerrc` est:
   
   ```
   machine projets.fsg.ulaval.ca/git api bitbucket
   ```
	
2. Créer dans votre répertoire personnel un fichier
   [`.netrc`](https://everything.curl.dev/usingcurl/netrc) contenant
   vos informations d'authentification au référentiel: nom d'hôte
   (uniquement); nom d'utilisateur; mot de passe. L'utilitaire curl
   utilisera ces informations pour se connecter au serveur.

   L'entrée du fichier `.netrc` pour le [référentiel
   BitBucket](https://projets.fsg.ulaval.ca/git) de la Faculté des
   sciences et de génie de l'Université Laval est de la forme
   suivante: 
   
   ```
   machine projets.fsg.ulaval.ca login IDUL password NIP
   ```
	
   Pour des raisons de sécurité, n'oubliez pas de restreindre l'accès
   à ce fichier à vous-même:

   ```
   chmod 400 ~/.netrc
   ```

Tous les exemples de commandes dans la suite supposent que la
configuration ci-dessus a été effectuée.


# Étape 1 --- Préparer le répertoire de correction et les outils

Roger utilise un ensemble de fichiers --- le *matériel de correction*
--- et suppose que chaque travail à corriger se trouve dans son propre
répertoire. 

1. Créer un répertoire de correction sur votre poste de travail pour
   accueillir la correction d'une évaluation.
   
2. Déposer dans ce répertoire le matériel de correction. Celui-ci est
   généralement fourni par la personne responsable de l'évaluation.

3. Faire du répertoire le répertoire courant à la ligne de commande.

4. Vérifier que les outils requis pour la correction énumérés dans le
   fichier `requirements.txt` (commandes et paquetages R, le cas
   échéant) sont disponibles sur votre poste de travail:
     
   ```
   roger checkreq
   ```


# Étape 2 --- Récupérer les travaux

La première étape de correction consiste naturellement à récupérer les
travaux. Lorsqu'ils sont hébergés dans un référentiel Git, cela passe
par le clonage de tous les dépôts. Ceux-ci sont normalement regroupés
dans un *projet*, un concept offert par
[BitBucket](https://support.atlassian.com/bitbucket-cloud/docs/group-repositories-into-projects/)
et [GitLab](https://docs.gitlab.com/ee/user/project/), notamment. Un
même projet peut contenir les dépôts de plusieurs évaluations.

L'outil `clone` de Roger permet de cloner l'ensemble des dépôts en une
seule commande. Il prend en arguments: 

- un motif d'expression régulière décrivant les noms des dépôts de
  l'évaluation;
- le nom du projet contenant les dépôts.

Le clonage crée un répertoire par dépôt dans le répertoire de
correction.

Dans notre exemple, le projet `foobar` regroupe toutes les évaluations
d'un cours. La commande à utiliser pour cloner les dépôts de
l'évaluation dont le nom correspond au motif `[0-9]{9}_[Ll]abo` est:

```
roger clone '[0-9]{9}_[Ll]abo' foobar
```


# Étape 3 --- Effectuer la correction automatisée

Le cœur du processus de correction avec Roger est la correction des
critères objectifs à l'aide de l'outil `grade`.

Le matériel de correction suivant doit se trouver dans le répertoire
de correction:

- le fichier de configuration de la correction `gradeconf` ou le
  fichier spécifié avec l'option `-c`;
- les fichiers de tests unitaires spécifiés dans le fichier de
  configuration.
  
L'option `-c` est particulièrement utile lorsqu'une évaluation compte
plusieurs fichiers de configuration différents à raison d'un par phase
du travail.

Une évaluation comporte habituellement une date et une heure limite de
remise. L'option `-l` permet de spécifier celles-ci et, ainsi, de
s'assurer de corriger les dépôts dans leur état à la date limite.

Si une correction manuelle est nécessaire, il convient d'utiliser
l'outil avec l'option `-d` afin que les dépôts demeurent dans l'état
dans lequel ils ont été corrigés automatiquement. Autrement, les
dépôts reviennent à leur état courant (`HEAD`) après la correction et
les fichiers visibles dans les répertoires peuvent différer de ceux
qui ont fait l'objet d'une correction.

Pour le reste, le seul argument obligatoire de l'outil `grade` est la
liste des répertoires contenant des travaux à corriger. 

Supposons qu'une correction manuelle suivra la correction automatisée
dans notre exemple. La commande suivante --- qui a recours à un
[glob](https://en.wikipedia.org/wiki/Glob_(programming)) pour
identifier les répertoires à corriger --- permet d'effectuer la
correction de tous les dépôts d'un coup:

```
roger grade -d -l "2042-10-11 23:59:59" [0-9]*/
```

La commande ci-dessus affiche les résultats à l'écran (ou à la *sortie
standard*, pour utiliser la terminologie Unix). C'est utile --- et
recommandé --- pour vérifier que la correction se déroule normalement.

Pour plutôt sauvegarder la correction dans un fichier `CORRECTION.txt`
dans chaque dépôt, il suffit d'ajouter l'option `-o` à la commande
précédente:

```
roger grade -d -l "2042-10-11 23:59:59" -o CORRECTION.txt [0-9]*/
```

> Lorsque les travaux sont hébergés dans des dépôts Git, Roger doit en
> tout temps avoir accès au référentiel durant la correction
> automatisée.


# Étape 4 --- Effectuer la correction manuelle

Le cas échéant, vous pouvez ensuite passer à la correction des
critères subjectifs ou qui nécessitent de juger de la qualité et de la
pertinence d'une réponse. Indiquez les résultats dans les fichiers de
correction spécifiés à l'étape précédente (`CORRECTION.txt` dans
l'exemple).

Si vous avez des commentaires à ajouter, veillez à les inscrire dans
la section `## Commentaires` en fin de fichier **et non au travers des
résultats**.


# Étape 5 --- Recueillir et consigner les résultats

La correction est maintenant terminée. Il faudra sans doute consigner
les notes dans un tableur ou dans un système de gestion de la
formation. C'est ici qu'intervient l'outil `harvest`.

Par défaut, l'outil `harvest` utilise les informations du fichier de
configuration `gradeconf` pour recueillir les notes dans tous les
fichiers `CORRECTION.txt` et effectuer les sommaires appropriés
(utiliser les options `-c` et `-f` pour spécifier d'autres fichiers).

Les résultats sont affichés à l'écran en format
[CSV](https://fr.wikipedia.org/wiki/Comma-separated_values) afin de
pouvoir être facilement importés dans un tableur.

La commande suivante retourne les résultats de la correction effectuée
aux étapes précédentes:

```
roger harvest [0-9]*/
```


# Étape 6 --- Publier la correction

Ne reste plus qu'à publier la correction aux étudiantes et aux
étudiants. Roger a été conçu pour publier les fichiers de correction
dans une branche (nouvelle ou non) des dépôts. C'est le rôle de
l'outil `push`.

Encore ici, l'outil `push` publie par défaut les fichiers
`CORRECTION.txt` (utiliser autrement l'option `-f`). 

L'outil prend en arguments: le nom d'une branche dans laquelle
publier; la liste des dépôts à publier. Si vous souhaitez publier dans
une nouvelle branche (une bonne idée), il faut aussi utiliser l'option
`-c`.

Si vous ne le modifiez pas avec l'option `-m`, l'outil utilisera un
message de validation par défaut.

La commande suivante publie les résultats de la correction effectuée
précédemment dans une nouvelle branche `correction`, et ce, pour
chacun des dépôts:

```
roger push -c correction [0-9]*/
```


# Étape 7 --- Rétablir l'état des dépôts

L'utilisation de l'outil `grade` avec les options `-l` et `-d`, à
l'étape 3, a fait en sorte qu'une version spécifique de chacun des
dépôts a été extraite et que les dépôts sont demeurés dans un état dit
«HEAD détachée». Il est généralement souhaitable de les ramener à leur
état courant dans la branche principale (`main` ou `master`). L'outil
`switch` permet de le faire en lot:

```
roger switch main [0-9]*/
```
