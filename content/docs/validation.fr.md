---
title: Guide de validation avec Roger
summary: Destiné aux étudiants et étudiantes, le guide de
  validation explique comment effectuer la validation d'un travail
  avec Roger.
type: documentation
toc: true
---

L'outil `validate` du système Roger permet aux étudiants et étudiantes
de vérifier que leurs fichiers satisfont les principales exigences
d'une évaluation *avant* d'en effectuer la remise.

Avant toute chose, soulignons que la documentation de l'outil de
validation est très complète. En cas de doute sur son utilisation,
n'hésitez donc pas à utiliser
```
roger validate --help
```


# Fonctionnalités

Si les fichiers de l'évaluation sont hébergés dans un dépôt Git,
l'outil peut d'abord effectuer les vérifications suivantes sur l'état
du dépôt tel que publié sur `origin`:

- le dépôt se trouve dans le bon projet;
- le nom du dépôt correspond à un motif spécifique;
- le dépôt n'est pas vide;
- la branche principale (`main` ou `master`) existe;
- une étiquette spécifique existe.

D'autres vérifications communes sont l'affichage de la liste des
auteurs et le calcul du nombre d'archives dans le dépôt.

> Insistons: c'est le dépôt **publié sur `origin`** qui fait l'objet
> des vérifications ci-dessus, et non le dépôt local. Pensez-y: c'est
> bien le premier, et non le second, qui sera corrigé!

Les vérifications effectuées sur le contenu d'un répertoire varient
d'un projet à l'autre. Pour des fichiers de texte brut ou de code
informatique, les vérifications les plus usuelles sont les suivantes:

- le fichier existe et n'est pas vide;
- le fichier est sous suivi par Git, le cas échéant;
- le codage de caractères du fichier est 
  [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange) 
  ou
  [UTF-8](https://fr.wikipedia.org/wiki/UTF-8);
- pour les fichiers de code informatique, le code est portable et
  analysable dans le langage du code source.


# Fichier de configuration

L'outil `validate` nécessite un fichier de configuration fourni par la
personne responsable d'une évaluation. Nommé par défaut
`validateconf`, ce fichier précise le nom et la nature de chaque
fichier à remettre et la structure du dépôt Git, le cas échéant.

Si le nom du fichier est différent ou s'il existe plusieurs fichiers
de configuration pour une même évaluation, il faut alors en préciser
le nom avec l'option `-c` de l'outil.


# Utilisation

L'outil de validation de Roger s'emploie depuis une ligne de commande
Unix (Terminal dans macOS, Git Bash dans Windows).

1. Installer le
   [système de base de Roger](https://roger-project.gitlab.io/download) 
   sur votre poste de travail, si ce n'est déjà fait.

2. (**Fortement recommandé**) Créer un répertoire pour accueillir les
   fichiers de l'évaluation.
   
3. Faire du répertoire contenant les fichiers de l'évaluation le
   répertoire de travail à la ligne de commande.

4. Enregistrer le fichier de configuration `validateconf` dans le
   répertoire contenant les fichiers de l'évaluation. La structure du
   répertoire est alors la suivante:
   
   ```C
   .
   |- foo.R
   |- bar.txt
   |- validateconf
   ```

5. Lancer l'outil `validate` avec en argument le nom du répertoire
   contenant le travail à valider. Si vous avez suivi les étapes
   ci-dessus, il s'agit du répertoire de travail courant et l'argument
   peut alors être omis:
   
   ```
   roger validate
   ```


# Exemples additionnels

1. Valider le travail dans le répertoire de travail courant avec le
   fichier de configuration `validateconf-conception`:

    ```
    roger validate -c validateconf-conception
    ```


2. Valider le travail dans un sous-répertoire `labo` du répertoire de
   travail courant. La structure du répertoire est alors la suivante:

   ```C
   .
   |- validateconf
   |- labo/
       |- foo.R
       |- bar.txt
   ```

   ```
   roger validate labo
   ```

3. Valider le travail dans le répertoire de travail courant avec le
   fichier de configuration situé dans le répertoire parent. La
   structure du répertoire est alors la suivante:

   ```C
   ..
   |- validateconf
   |- .
      |- foo.R
      |- bar.txt
   ```

   ```
   roger validate -c ../validateconf
   ```


# Validation dans R

Si le [paquetage R **roger**](https://cran.r-project.org/package=roger) 
est installé sur votre poste de travail, vous pouvez aussi lancer la
validation directement dans R avec la fonction `roger_validate`. 

```R
> library("roger")
```

En supposant que le répertoire courant est le répertoire de travail de
R, les commandes équivalentes aux quatre exemples ci-dessus sont les
suivantes.

1. Validation du travail dans le répertoire de travail avec le fichier de
   configuration par défaut:
   ```R
   > roger_validate()
   ```
   
2. Validation du travail dans le répertoire de travail avec le fichier de
   configuration `validateconf-conception`:
   ```R
   > roger_validate(config_file = "validateconf-conception")
   ```

3. Validation du travail dans le répertoire `labo` avec le fichier de
   configuration par défaut:
   ```R
   > roger_validate("labo")
   ```

4. Validation du travail dans le répertoire de travail  avec le fichier de
   configuration `validateconf-conception` dans le répertoire parent:
   ```R
   > roger_validate(config_file = "../validateconf-conception")
   ```
